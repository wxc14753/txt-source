# novel

- title: 宝くじで40億当たったんだけど異世界に移住する
- title_zh: 中了40億圓樂透的我要搬到異世界去住了
- author: すずの木くろ
- illust: 黒獅子
- source: http://ncode.syosetu.com/n2163n/
- cover: https://i.ibb.co/m4kvcm0/23-9413.jpg
- publisher: syosetu
- date: 2019-06-09T18:08:00+08:00
- status: 連載
- novel_status: 0x0300

## illusts

- 今井ムジイ

## publishers

- syosetu

## series

- name: 宝くじで40億当たったんだけど異世界に移住する

## preface


```
偶尔买的乐透中了40亿元，
　　一夕致富的志野一良为了逃避闻到钱味而聚集的鬣狗们，躲到祖传的乡下老房子避难，没想到那房子居然和某个发生大饥荒的异世界相连！
　　一良在异世界遇见了美丽的少女薇蕾塔，决定帮她拯救村子。结果一良的名声渐渐传开，传到领主耳中——
　　穿梭于现实世界与异世界之间，以金钱的力量发展异世界，时而送入物资，时而送入技术，一良不断在异世界行善救人。

試しに買ってみた宝くじで４０億円の高額当選を引き当てた一良。
どこからか金の臭いを嗅ぎつけたハイエナ共から逃げるため、一良は先祖代々から伝わる古い屋敷に避難する。
一良が避難先である屋敷の中を調べていると、とある一室が異世界と行き来できる空間であることを発見する。
文化レベルや技術レベルがかなり低いとみられる異世界に、主人公は時に品物を、時に技術を持ち込み、その世界で自分の価値を見出そうとする。

※双葉社の新レーベル　モンスター文庫にて書籍化しました。
　コミックウォーカーにてコミカライズ版連載中です。
```

## tags

- node-novel
- R15
- syosetu
- その他
- ファンタジー
- ローファンタジー
- ローファンタジー〔ファンタジー〕
- 内政・経済
- 恋愛
- 技術供与
- 残酷な描写あり
- 物資供与
- 現代知識
- 異世界転移

# contribute

- kerorokun
- 閑基
- 千里朱音
- 不息不止
- 不捨得拆書辛苦表妹修圖湊活看吧
- 自力更生
- 愛莉小天使
- linpop
- slashloaf
- lasthm
- 哈迪斯兜帽
- 水無月流歌
- 小长颈鹿脖子痛
- 佐藤14
- a4605683
- 

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n2163n

## wenku8

- novel_id: 1866

## textlayout

- allow_lf2: false

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n2163n&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n2163n/)
- [中了40亿的我要搬到异世界去住了吧](https://tieba.baidu.com/f?kw=%E4%B8%AD%E4%BA%8640%E4%BA%BF%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E5%BC%82%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86&ie=utf-8 "中了40亿的我要搬到异世界去住了")
- http://www.dm5.com/manhua-zhongle-40-yidewoyaobandaoyishijiequzhule/
- https://drive.google.com/file/d/1ag6RM4hZBw0iQEIMzkmLYz-ISfFa4JSn/edit a4605683

