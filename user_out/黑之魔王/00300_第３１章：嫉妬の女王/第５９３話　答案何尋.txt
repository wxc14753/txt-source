我察覺到附近有人在活動後，醒了過來。

「早上好，哥哥」

我以為我看見了天使。

不，不對。雖然像天使一樣可愛，但是他並沒有長著翅膀，頭上也沒有光環。那細長的特徵性的耳朵，正是精靈的標誌。

「⋯⋯西蒙，嗎，怎麼了？」
「什麼怎麼了，你受了這麼重的傷，肯定要人照顧吧」

我反射性地打算起身，但從那只傳來鈍痛的右手和右腳的感覺後，回想起了自己身體的狀態。
對了，我現在還需要別人的照顧。

今天是新陽の月最後的一天，３０號。
昨天傍晚，我終於回到了斯巴達。救護馬車本來預定是去斯巴達大神殿的，但是現在就只需要等傷自然恢復的我，根本不需要住院。因此，我徑直回到了自己的家──被詛咒的宅邸。

西蒙在那裡迎接了我。
他只笑著說道「你回來了」，卻什麼都沒有問我。
我在住院生活之中，已經把我與混沌熊兔的戰鬥，以及受的這重傷，分別寫信告訴了西蒙和威爾。雖然在傷好之前會晚點回家，但是不用擔心。

我只寄了這那封信⋯⋯但在莉莉的使者艾因來訪的第二天，菲奧娜悄悄地把寫了一定程度情況的信送給了西蒙。
不問我的原因，是因為已經靠菲奧娜的信知道了大概的情況吧。

「但是，西蒙也不用特意來照顧我吧」

現在我是躺在自己房間的床上睡到了早上。因此，西蒙似乎正在準備我的換洗衣服和洗漱用具。
昨天我在稍微說了些無關緊要的話後，就馬上睡著了，所以我以為西蒙會回去的⋯⋯原來他一開始就是以照顧我的前提過來的啊。

「菲奧娜桑的信上寫著『請多照顧哥哥，只有小柩的話還是讓人放不下心』」

連這種事都寫了嗎？

但是，讓既不是潘多拉神殿的神官又不是治癒術士的西蒙來照顧受傷的人又算怎麼一回事啊？雖然在值得信賴這點上是最棒的人選，但是西蒙也很忙吧，要是這份工作還有其他合適的人選⋯⋯沒有。我根本不認識擅長這種事的熟人。

難道是我交友關係太狹窄了？我，是斯巴達的英雄吧。難道我覺得我是英雄一事，只是我自我意識過剩嗎？我不禁這麼想。

「這樣啊⋯⋯那麼，就拜託你了」
「恩，雖然我不是很擅長這事，但我會努力的。」

看到這樣笑著回答的西蒙，我的心臟一緊。

其實，已經不需要看護了。我現在就能治好這個傷。
因為我已經得到了第六加護。

使用這個加護的話，我的手腳就會瞬間恢復原樣。雖然並不能直接就全力戰鬥，不過，還是能自由活動吧。
可以用自己的腳來移動。也就是說，我能去莉莉哪裡。

但是，我還沒有使用，在剛才的夢中得到的新加護。我不想用它。我還想繼續當傷員。
為什麼，為什麼要保持這種廢物狀態呢⋯⋯一定是因為我還無法做出決定。
我到底該怎麼辦？

莉莉抓住了菲奧娜、沙利叶、妮露三人為人質。
因此，米婭說殺了她救出三人。殺一個人，救三個人。還可以通過最後的試練。

但是，這可是人的生命啊，更何況還是自己的重要的人，不可能僅僅憑數量大小來判斷吧。
殺掉莉莉？別開玩笑了。如果說這是最後的試練的話，那我永遠都不會想去通過這種試練。就算是魔王的加護，即使因此失去前六個加護也無所謂。我不會有任何留戀。想變強的話，用別的方法重新鍛鍊就可以了。

那麼，要幫助莉莉嗎？
所謂幫助，到底是怎樣才能救出她呢？
莉莉她已經按照她自己的意思，抓了三個人質。

菲奧娜她們應該是以殺掉莉莉為目的去挑戰莉莉的。但是，她擊敗了這三人，並活捉了她們。該讓人覺得可怕的是她那戰鬥能力嗎？不，是她那即使同時與三人為敵，也選擇生擒的判斷力。

恐怕莉莉知道著人質的價值。是的，她知道的。菲奧娜把沙利叶作為人質，讓她對我的告白成功了。
莉莉的目的一定就是這個。以三人性命為質，逼迫我只愛她一人。
然後，在我接受了這事後，我就會和莉莉結婚，永遠的在《樂園》裡和她幸福的過著二人世界──這樣好嗎？

也許不錯。

我喜歡莉莉。非常喜歡。她是我賭上性命，也想守護的重要存在。如果讓我必須在陌生的一百人的性命和莉莉一個人的性命做出選擇的話，我會一邊痛苦一邊毫不猶豫的選擇莉莉。

如果能和她這樣的女朋友交往的話，不管對我有多少限制，我都會原諒她，接受她。

「與菲奧娜分手，與莉莉交往，嗎⋯⋯太差勁了啊，我」

但是，只要我成為最差勁的男人，就能救下所有人的性命的話，這已經很划算了吧。能用我的一顆心，換下莉莉，菲奧娜，沙利叶和妮露的話，就算是何種屈辱我都能甘心接受。我甚至願意去舔哪骯髒的鞋底。

接受莉莉的婚約，解放三名人質，就算會讓她們哭泣，這也是最好的選擇了吧。我不會再和她們見面了。只會聽莉莉得話，發誓這一生只愛莉莉一人。

這樣的話，菲奧娜她們能平安無事的獲救嗎⋯⋯以聰明的莉莉來說，如果我如此乞求的話，她會明白這三個人質已經毫無價值，然後放掉她們吧。莉莉想要的是我。並不是想殺掉那三個人。三人的性命，不過是為了得到我這個男人的手段而已，只是用來當人質而已。如果只是為了泄憤而殺掉了這三個人的話，我要麼會被莉莉失手錯殺，或者會繼續戰鬥直到殺掉莉莉為止吧，但不管怎樣，她都得不到我。

所以，只要讓我立下愛之誓的話，就算莉莉贏了。

「⋯⋯我本來應該選菲奧娜的」

不管經過如何，我都接受了菲奧娜的告白。

愛著我的莉莉，和說出愛我的菲奧娜。在我心中兩人組成的天秤，因為菲奧娜的那句，因為愛你，所以我原諒你了，而傾向了她。所以，我選擇了菲奧娜。我打算和她結婚，相守一生。

這就是，我無法決定接受莉莉的愛的原因。

如果和莉莉重逢，我會說什麼呢？我一直在想著這事。但我卻想不出什麼話好說，但我會既不欺騙也不隱瞞的對她說「我愛菲奧娜。我和她在以結婚為前提的交往著」

我不知道。聽了這話後。莉莉會做出怎樣的判斷，因為從這開始，就由她自己決定怎麼辦了。

儘管如此，是作為『元素支配者』的成員在一起，還是，解散隊伍。四散分離嗎？也許我什麼答案都找不到，也許我永遠都無法下定決心。所以，就算莉莉對我生氣也好。只要不殺了我，即使會砍斷我手腳，我也做好了精神準備。

「結果，我的決心，就這點程度嗎⋯⋯」

如果說我真正地愛著菲奧娜，就應該毫不迷茫地殺掉莉莉，救出菲奧娜。
但是，現在我卻並沒有打算這麼做，最終還是覺得背叛菲奧娜，從了莉莉才是最好的選擇。

「可惡，但是⋯⋯這與性命攸關啊⋯⋯到這種地步，還應該貫徹愛情嗎？」

為了最愛的人，毫不猶豫地殺掉其他重要的人。
這真的是愛嗎？至少在我的常識中，沒有這種邏輯啊。

但是，莉莉和菲奧娜都是這麼做的。
菲奧娜為了保護我，去殺莉莉。
沙利叶也贊同了她。甚至連妮露也覺得她是正確的。

沙利叶一定是單純地把莉莉判斷成莉莉會對我不利的敵人了吧。妮露也一定因為是相似的原因吧。從旁人看來，莉莉確實是在無視我的自由意識的基礎上，不管其他人的犧牲和受害，為了得到喜歡的男人而向我襲擊的瘋子。

即使認真地以斯巴達的法律來說，這也是不被允許的行為。當有人受害時，這就成為了罪，要被處罰。反過來說，她已經算是被騎士當場殺掉也無法抱怨的，純粹的罪犯了。
但說是罪人，她現在也只影響到身邊的人而已。實際上，如果菲奧娜她們殺了莉莉的話，那麼一切也都就結束了。到底誰會贏，米婭並沒有考慮到這一點。

如果菲奧娜真的殺了莉莉，我能原諒她嗎？那天晚上，要是如她所願的殺掉了莉莉，我能接受並和這樣的菲奧娜結婚嗎？
事到如今這也只不過是個假設了，儘管如此，我一定會原諒菲奧娜的。就算不能原諒，但還是會努力的原諒她。

菲奧娜有充足的殺死莉莉的理由。這我理解。但我也有僅憑道理無法割捨的感情。我無法直接割捨。我不可能承受莉莉死亡的情況，也不能容忍殺掉她的人物。
但是，因為是菲奧娜⋯⋯正因為是她，所以我才想原諒她，我必須去原諒她。

這到底是愛，還是單純的逃避，我無從得知。但即便如此，如果是菲奧娜的話，我還是會原諒她。恐怕，要是是沙利叶的話，我就不能原諒她吧。是妮露的話，也無法原諒她吧。雖然不會去報仇，但是我不會再和她們見面，一生都會躲著她們吧。

但是，要是菲奧娜的話我可以原諒她。
我想，這一定是因為我按著我的方式愛著她。

「如果愛你的話，就應該殺掉莉莉嗎？因為愛你，所以要為了救你而屈服嗎？」

我不知道哪個選擇才是對的。

要是我殺了莉莉並救出她的話，菲奧娜一定才是最開心的吧。比起莉莉，選擇了自己，自己更重要，自己才是最重要的，我的行為會比起任何甜言蜜語都要更直接的將這事傳達過去吧。

相反，如果我跟莉莉說要和菲奧娜分手，她會無比傷心。悲痛，也許會有無法用語言來形容的絶望吧。菲奧娜對我的思念有多么的深⋯⋯我一定還沒有理解她全部的愛，不過，儘管如此，在作為戀人度過的時光中，我觸摸到了她的愛的一部分。我自然地領悟到，我比自己想像的還要被她所愛著。

我已經明白，被愛，相愛，是多麼的舒適，多麼的美好，多麼的令人滿足。
我根本不想和菲奧娜分手。如果，有人要拆散我倆，我一定會以不惜幹掉對方的覺悟去阻止他吧。

但是，現實卻滿是惡意，並沒有妨礙我和菲奧娜的惡敵。擋在我們面前的人是莉莉。
但是，我不可能親手殺死莉莉莉。
但是，我也知道只要不殺了莉莉，她就不會停手。

如果，我能夠順利地不殺莉莉，並使她無力化，再救出三人。這樣的話，之後會變成什麼樣呢？
正如菲奧娜所說，莉莉可以無數次謀劃。更強，更過激。她為了得到我，會不惜任何犧牲的，發起襲擊。

但若是我屈服於莉莉，菲奧娜被順利放走的話，恐怕她也會不論花上多長時間，都會來奪回我吧。
沙利叶和妮露倒是只要她兩能活下來就行。沙利叶最近冒険者也做的有模有樣。她可以直接就去當冒険者去，也可以去開咖啡店，甚至隨便去某人家裡做女僕也行。即使我不在，沙利叶也能一個人走上新的人生吧。

妮露就更沒有必要擔心了。只要繼續做她的公主就行了。她不是一個可以因為這種情況，死在這種地方的人。
但菲奧娜⋯⋯如果我選擇莉莉而拋棄她的話，菲奧娜真的會忘記我走上新的人生嗎？

不，不會。菲奧娜一定會選擇對莉莉復仇。不論是耗盡一生，還是就算耗盡一生也徒勞無功，她都一定會繼續走著那徒勞的奪回我的復仇之路。
那麼，我救下菲奧娜性命還有意義嗎？即便只有萬分之一的機率，菲奧娜能夠輕易地說，她會忘記我然後幸福地生活下去嗎？

不行，我希望菲奧娜能夠幸福。不想她在充滿憤怒、悲傷和絶望的復仇人生路上走下去。這樣的話，就算讓她活下去了，也沒有意義了。

但是，要實現菲奧娜的幸福⋯⋯只有殺死莉莉。
陷入死循環了。
莉莉不能殺。菲奧娜不能死。儘管如此，我還希望莉莉和菲奧娜都能幸福。

找不到答案。也許根本就沒有答案。

「繼續思考啊⋯⋯現在還沒有人死」

只有現在，還有挽回的機會。

「救下所有人的方法，一定是存在的⋯⋯」

不要放棄。

莉莉也好，菲奧娜也好，都是我深愛的人。即使魔王要把殘酷的試練強加給我。

我也想救下所有人。我不希望任何人死掉。
我已經不想再失去任何一個人了。

「⋯⋯我只是害怕嗎？比起貫徹愛情，難道我只是在害怕失去嗎？」

也許就是這樣。
這種想法，是不好的嗎？不想失去重要的人，這不是每個人都理所當然的會祈禱的事嗎？

那麼，為什麼實現這理所當然的願望卻是如此的困難？
為什麼，在愛的名義下，她們能夠平靜地互相殘殺。

「愛到底是什麼啊⋯⋯」

我已經不知道自己在想什麼了。

但是，今天已經是新陽の月３０日了。

到莉莉預告的她來接我的初火の月之６號，只剩下一周時間了。
我連思考的時間都所剩無幾了。

───

初火の月２日。黒乃的宅邸，時間平靜的流逝著。

「怎麼樣，好吃嗎？」
「啊，很好吃哦」

午後，黒乃和西蒙兩人，坐在寬敞的餐廳吃著午飯。雖然黒乃不能用慣用手，但他只靠靈巧的左手就能順利的進食。因此不需要餵食，西蒙也一起坐在座位上吃著午飯。

現在的黒乃需要的與其說是身體上的護理，不如說是精神上的安寧吧。
了解大概情況的西蒙，表面上頂著照顧的名義，實際上他認為陪在黒乃身邊是自己的義序。

「啊，對不起，這裡有點烤焦了。」
「不，就是這裡好吃。」
「看來我不應該做自己不習慣的事情啊」
「你能做到如此程度已經算做的很好了。至少比我做得要好」
「因為我獨自生活了很長一段時間啊。這是最低限度的必學技能哦」

西蒙作為錬金術士，過著以研究為中心的生活。只要能填飽肚子，那吃什麼都行。所以他當然不會萌生磨練料理技巧的上進心⋯⋯西蒙重新想到，自己的事情怎樣都好。

（嗯，糟糕。這麼正常的回答，反而讓人覺得很糟糕）

西蒙推測黒乃的精神已經達到了極限。雖然自己絶不是能敏感感受別人心靈的微妙之處的人，但儘管如此，如果知道黒乃的性格和現在他這無可奈何的混亂的戀愛情況的話，還是能簡單察覺到他內心的深沉黒暗。我寧願自己知道地不這麼清楚。只是聽到這情況，我就覺得胃痛。

威爾倒是說。正是因為如此痛苦，才應該與魂之盟友分享，不過，他因為一些事被召回到斯巴達王城。對斯巴達的政治和內情並不了解的西蒙來說，並想不到讓威爾第二王子在神學院休學都要叫回去的原因。

在王城期間，很難直接和王子取得聯繫。不知道他什麼時候會回來。連他特意來跟我打招呼，說他暫時不在家，都算是尊貴的待遇了。
不管怎樣。沒有可以依賴的友人威爾。這樣的話，就只能靠西蒙一個人支撐黒乃了。

「我吃飽了」

黒乃的表情平靜，禮貌地說道，他看起來並不陰沉。臉色也不是特別憔悴。看不出深思苦慮的樣子，表情和往常一樣。
如果硬要找出差異的話，就是他的左眼戴著厚厚的黒色的布製眼罩。但我知道，這不是一種時尚打扮。

如果莉莉看到我和黒乃兩人在和諧地吃飯的情景，自己也會成為她的嫉妒對象吧。這讓我不由得感到恐怖。本來西蒙就算沒有現在這種情況，他也覺得莉莉十分可怕，難以對付。雖說她作為工作伙伴時，無比優秀，讓人不由得敬仰起了她，但儘管如此，可怕的東西就是很可怕啊。

「我們出去一會兒吧？今天天氣晴朗，很舒服哦」
「啊，拜託你了」

在黒乃的背後，像死神一樣全幅武裝的，可怕的盔甲正無聲矗立著。沒有精神耐力的西蒙只是看到這被詛咒的古代鎧甲，就覺得無比的恐怖。但這暴君之鎧說是會以女僕小柩的意志行動，她現在正是照顧黒乃的工作伙伴。

想要搬動黒乃那巨大的身軀，西蒙力氣還是太小了。在搬黒乃的時候，就會交給如看到的這樣，不，有著比外表更強大的力量的她。

按理來說，作為黒乃主人的第一女僕和女僕長的小柩，本會堅持說照顧工作就全部交給她吧，但因為她好像在介意自己違背黒乃意願強制把他送回斯巴達的事情。

「嗚，小⋯⋯小柩已經沒臉再見主人了！」

於是，她不情願地將黒乃交給西蒙照顧，並流著淚表明了自己堅定不移的決心，然後她就將體力活和幕後工作全都攬了下來。

西蒙十分吃驚的看到，從模仿著恐怖骷髏的眼眶中，竟然真的流出了像眼淚一樣的液體。
這個古代鎧甲到底是什麼構造啊？雖然西蒙很想拆開檢查下，但是如果這麼做的話就會被殺掉吧，所以只好暫時先把好奇心放在一邊了。

「我先去煮咖啡了啊。」
「麻煩你了」
「沒關係，現在這就是我的工作嘛。」

讓小柩把黒乃搬到陽光好的二樓陽台後，西蒙走去了廚房。自從沙利叶教會他沏咖啡的方法之後，他自己也拿了些咖啡豆來喝。西蒙做夢也沒想到，能和自己打算殺掉的狙擊對象進行這種交流⋯⋯對西蒙來說，沒有感情，只按理性行動的沙利叶，算是最好交往的啦。

總之，對於西蒙來說。最可怕的人就是莉莉啦。

「⋯⋯該怎麼辦啊」

西蒙一到廚房就抱起了腦袋。

他明白現在不是這麼悠閑度日的時候。黒乃正處在生死關頭。而且，還有著到初火の月６日的時間限制。

「傾聽也好，詢問也好，都要快點啦。」

西蒙還沒有聽過黒乃的答案。他要怎麼辦呢？他想怎麼做呢？

自己一直沒有詢問。在讀完菲奧娜的來信後，他就想，自己決不會做出追問似的舉動。
無論是殺死莉莉也好，還是拋棄菲奧娜也好，如果是黒乃的話，他肯定會自己找到答案的。

但是，現實並不是那麼簡單的東西。
在他身邊照顧的話，除了感到擔心以外，什麼都說不出來。自己完全想不出該怎麼開口

「不過，果然還是早點告訴他比較好呢⋯⋯」

從菲奧娜那寄來的信有兩封。

第一封是她在從阿瓦隆出發之前送出來的。在飛龍快遞下，很快就送到了西蒙手中。

信的內容是莉莉要以結婚為名，來監禁黒乃。為了阻止此事，菲奧娜，沙利叶和妮露三人要去挑戰莉莉。莉莉的根據地是神滅領域・阿瓦隆的無人踏足過的領域。然後，因為黒乃拼死反對這個行動，所以把黒乃送回了斯巴達，在菲奧娜她們回來之前就交給西蒙照顧了。

讀著這封信，西蒙因為這絶望的內容，感到眼前一片漆黒，但菲奧娜對西蒙的試練，卻還沒有結束。
在黒乃抵達斯巴達的前一天。第二封信寄到了西蒙手中。

「您是西蒙・弗里德里希・巴爾緹艾爾大人吧。我的名字叫薇薇安。是菲奧娜大人的使魔」

第二封信件，形狀是妖精的模樣。
或者說，是真正的妖精。

妖精頭上戴著項圈型的魔術道具、手腳和背部都刻著幾種刻印組合的術式。精通魔法術式的西蒙一眼就察覺到了，這是和莉莉的思考制御装置不同系統的隷属術式。雖然是一半左右的部分自己都無法解讀的未知術式⋯⋯但是支配在人活著的情況下，把他變成使魔的魔法，總的來說就算是外法，禁術之類的東西了。

但是，協助製作思考制御装置的西蒙，事到如今，也並不會認為這是犯了使用支配人類的禁術的罪，並去告發給騎士團了。繼莉莉之後，就連菲奧娜也這麼做了，對於她掌握著作為人不被允許的惡魔魔法這件事，他只產生了「啊，果然如此」的感想。

先將違法使魔的薇薇安的處理方法放在一邊，重要的是她帶來的情報。
她能用她的心靈感應能力，將菲奧娜和莉莉戰鬥的記錄，播給別人看。

西蒙覺得自己沒有先於黒乃觀看菲奧娜壯烈的戰鬥結局的權利，所以沒有看。但是，他明白菲奧娜派來這個薇薇安的意義。

「菲奧娜桑正在尋求幫助。」

菲奧娜無視黒乃的反對，前去殺死莉莉。但是，她為了防備萬一自己力所不及失敗的情況，所以她留下了薇薇安。
她相信黒乃一定會來救她。所以為了打倒莉莉，自己至少應該留下些許線索。

「果然，就算只是薇薇安的事情，也必須要告訴黒乃」

即使黒乃知道了這件事，事到如今，這也並不是什麼大不了的事。因為黒乃已經知道菲奧娜她們被莉莉囚禁了，所以即使看到她們敗北的過程，也不會找到答案的暗示。菲奧娜留下的不是如黒乃希望的那樣，解決一切事情的情報，而是只對殺死莉莉有用得戰鬥記錄⋯⋯

如果看黒乃看了這東西，明顯只會讓他變得比現在更要痛苦煩惱吧。
但是，既然自己受到了這東西，那麼讓黒乃看這個就是西蒙的義務，同時，黒乃也有看這個的義務。

「好，去說吧！」

西蒙鼓起幹勁，拿著咖啡朝陽台走去。

「──風也變冷了，我們回去吧。」
「啊，嗯⋯⋯」

結果，沒能說出來。

西蒙看著喝著咖啡賞著黃昏的黒乃，突然感到了不安，如果說出薇薇安的記錄，他是不是會決定對著眼前這尖銳的柵欄跳樓自殺呢？
不，雖然知道以黒乃的身體，就算底下有利刃，也不會讓他受致命傷，但問題不在於肉體，而在於精神。

「啊，不行！必須在今天之內盡快說出來！」

但是，迫近的期限已經讓自己無法再推遲公布薇薇安的情報了。
西蒙十分的苦惱。他說著要準備晚飯，回到廚房中繼續煩惱著。

他一邊嗚嗚呻吟著，一邊煩惱著，但天才錬金術師終於靈光一閃。

「好，這種時候，就借助一下酒的力量吧！」

西蒙靠著這十分簡單的手段，挑戰與黒乃共進晚餐。